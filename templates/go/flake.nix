{
  description = "A dev environment for a Go project";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  inputs.utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system: {
    devShells.default = with nixpkgs.legacyPackages.${system};
      mkShell {
        shellHook = ''
          export GOPATH="$(git rev-parse --show-toplevel)/.go"
          export GOSUMDB=sum.golang.org
          export GOPROXY=https://proxy.golang.org,direct
        '';
        packages = [
          go
          gopls
        ];
      };
  });
}
