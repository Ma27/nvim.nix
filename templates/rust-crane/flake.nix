{
  description = "An environment for rust with crane";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
      inputs.flake-compat.follows = "flake-compat";
      inputs.rust-overlay.follows = "rust-overlay";
    };

    # Temporary until https://github.com/NixOS/nix/issues/6986 is solved.
    flake-utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, crane, ... }: let
    inherit (nixpkgs) lib;
    forEachSystem = lib.genAttrs [ "x86_64-linux" ];

    inherit (with builtins; (fromTOML (readFile ./Cargo.toml)).package) name;

    craneArtifacts = forEachSystem (system: let
      craneLib = crane.lib.${system};
      common = {
        pname = name;
        src = craneLib.cleanCargoSource self;
      };
      pkgs = nixpkgs.legacyPackages.${system};
    in rec {
      cargoArtifacts = craneLib.buildDepsOnly (common // { doCheck = false; });

      clippy = craneLib.cargoClippy (common // {
        inherit cargoArtifacts;
        cargoClippyExtraArgs = "--all-targets -- -D warnings";
      });
      format = craneLib.cargoFmt (common // { inherit cargoArtifacts; });

      ${name} = craneLib.buildPackage (common // {
        inherit cargoArtifacts;

        shellHook = ''
          export PATH="${lib.makeBinPath (with pkgs; [ clippy rustfmt rust-analyzer rustc ])}:$PATH"
        '';
      });
    });
  in {
    packages = forEachSystem (system: (lib.fix (packages: {
      ${name} = craneArtifacts.${system}.${name};
      default = packages.${name};
    })));
    checks = forEachSystem (system: {
      inherit (craneArtifacts.${system}) format clippy;
    });
  };
}
