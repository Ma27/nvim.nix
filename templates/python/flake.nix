{
  description = "A dev environment for a Python project";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  inputs.utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
      inherit (nixpkgs.legacyPackages.${system}) python3 ruff;
      fDeps = ps: [ /* your project dependencies here */ ];
      devTools = with python3.pkgs; [
        pytest python-lsp-server pylsp-mypy mypy ruff python-lsp-ruff
      ];
    in {
      devShells.default = with nixpkgs.legacyPackages.${system};
        mkShell {
          packages = [ (python3.withPackages fDeps) ]
            ++ devTools;
        };
      packages.default = python3.pkgs.buildPythonPackage {
        pname = "build";
        src = self;
        propagatedBuildInputs = fDeps python3.pkgs;
        nativeCheckInputs = devTools;
      };
    });
}
