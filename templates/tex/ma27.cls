\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ma27}

\LoadClass[openany,parskip,a4paper,twoside]{scrbook}

%% Encoding
\usepackage[T1]{fontenc}
\usepackage[german]{babel}
\usepackage{color} 
\usepackage[utf8]{inputenc}
\usepackage{csquotes}
\usepackage{subfigure}
\usepackage{todo}

%% Hyperlinks
\usepackage{hyperref}
\hypersetup{pdftitle=Test \LaTeX Thingies, colorlinks=true}
\usepackage{imakeidx}

\makeindex[title=Glossar,columns=3]

%% Tabs
\usepackage{booktabs}% http://ctan.org/pkg/booktabs
\newcommand{\tabitem}{~~\llap{\textbullet}~~}

%% Code
\usepackage{listings}                                 % Syntax highlighting
\usepackage[ruled,vlined,linesnumbered]{algorithm2e}  % Pseudocode

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\definecolor{ferngreen}{rgb}{0.31, 0.47, 0.26}
\lstset{frame=tb,
  backgroundcolor=\color{backcolour},
  commentstyle=\color{codegreen},
  keywordstyle=\color{magenta},
  numberstyle=\tiny\color{codegray},
  stringstyle=\color{codepurple},
  basicstyle=\ttfamily\footnotesize,
  breakatwhitespace=false,
  breaklines=true,
  captionpos=b,
  keepspaces=true,
  numbers=left,
  numbersep=5pt,
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  tabsize=2
}

%% Math
\usepackage{amssymb}    % additional symbols
\usepackage{amsmath}    % math expressions
\usepackage{amsthm}     % newtheorem

% pgfplots - Plotter
\usepackage[margin=0.25in]{geometry}
\usepackage{pgfplots}
\pgfplotsset{width=10cm,compat=1.9}

%% Layout
\usepackage{multicol}                     % Multi-column List
\usepackage{wrapfig}		                  % Floating layout around a figure
\usepackage{fullpage}                     % Better margin
\usepackage{thmtools}                     % Md Style
\usepackage[framemethod=TikZ]{mdframed}   % Text Blocks
\usepackage{zref}

%% Boxed Textblocks
\newmdtheoremenv[nobreak=true]{stz}{Satz}
\newmdtheoremenv[nobreak=true]{bew}{Beweis}
\newmdtheoremenv[nobreak=true]{defi}{Definition}

\declaretheoremstyle
[
    spaceabove=0pt, spacebelow=0pt, headfont=\normalfont\bfseries,
    notefont=\mdseries, notebraces={(}{)}, headpunct={\newline}, headindent={},
    postheadspace={ }, postheadspace=4pt, bodyfont=\normalfont, qed=$\blacktriangle$,
    preheadhook={\begin{mdframed}[style=myframedstyle]},
    postfoothook=\end{mdframed},
]{mystyle}

\declaretheorem[style=mystyle,numberwithin=chapter,title=Anmerkung]{anmerkung}
\mdfdefinestyle{myframedstyle}{
    outermargin = 1.3cm ,
    leftmargin = 0pt , rightmargin = 0pt ,
    innerleftmargin = 5pt , innerrightmargin = 5pt ,
    innertopmargin = 5pt, innerbottommargin = 5pt ,
    backgroundcolor = blue!10,
    align = center ,
    nobreak = true,
    hidealllines = true,
    topline = true , bottomline = true ,
    splittopskip = \topskip , splitbottomskip = 0pt ,
    skipabove = 0.5\baselineskip ,  skipbelow = 0.3\baselineskip}

%% Custom commands

%%% Init ToC

\newcommand{\mytoc}{
\maketitle
\renewcommand{\thechapter}{\Roman{chapter}}
\renewcommand{\chaptername}{§}
\addcontentsline{toc}{chapter}{Inhaltsverzeichnis}
\tableofcontents
}

%%% Skript Verlinkung

\newcommand{\registerskript}[3]{
\newcommand{#1}[1]{\href{run:\detokenize{#2}}{#3 (##1)}}
}

%%% Simpler Math things
\newcommand{\bb}{\mathbb}
\newcommand{\R}{\bb{R}}
\newcommand{\C}{\bb{C}}
\newcommand{\Q}{\bb{Q}}
\newcommand{\N}{\bb{N}}
\newcommand{\Z}{\bb{Z}}
\newcommand{\K}{\mathbb{K}}

\newcommand{\eq}{\Longleftrightarrow}
