{
  description = "A build environment for TeX book / document";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  inputs.utils.url = "github:numtide/flake-utils";
  inputs.tex2nix = {
    url = "github:mic92/tex2nix";
    inputs.nixpkgs.follows = "nixpkgs";
    inputs.flake-utils.follows = "utils";
  };

  outputs = { self, nixpkgs, tex2nix, utils }: utils.lib.eachDefaultSystem (system: {
    devShells.default = with nixpkgs.legacyPackages.${system};
      mkShell {
        packages = [
          self.packages.${system}.texlive
          zathura
        ];
      };

    apps.update = with nixpkgs.legacyPackages.${system}; {
      type = "app";
      program = "${writeShellScript "tex-update" ''
        mapfile -t TEX_FILES < <(find . -type f -iname '*.tex' -or -iname '*.cls')
        exec ${tex2nix.packages.x86_64-linux.tex2nix}/bin/tex2nix "''${TEX_FILES[@]}"
      ''}";
    };

    packages = {
      default = self.packages.${system}.document;
      texlive = with nixpkgs.legacyPackages.${system};
        callPackage ./tex-env.nix {
          extraTexPackages = {
            inherit (texlive) latexmk preprint;
          };
        };
      document = with nixpkgs.legacyPackages.${system};
        stdenv.mkDerivation (finalAttrs: {
          pname = "document";
          version = self.lastModifiedDate or "dirty";
          src = self;
          nativeBuildInputs = [
            self.packages.${system}.texlive
          ];
          buildPhase = ''
            runHook preInstall
            export HOME="$(mktemp -d)"
            [ -f Index.tex ] || {
              echo "No root (Index.tex) found, aborting!"
              exit 1
            }
            latexmk -pdf Index.tex -interaction=nonstopmode
            runHook postInstall
          '';
          installPhase = ''
            runHook preInstall
            install -Dm0444 Index.pdf $out/Index.pdf
            runHook postInstall
          '';
        });
    };
  });
}
