{
  description = "A dev environment for a Rust project";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  inputs.utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system: {
    devShells.default = with nixpkgs.legacyPackages.${system};
      mkShell {
        packages = [
          rustc
          cargo
          rustfmt
          clippy
          rust-analyzer
        ];
      };
  });
}
