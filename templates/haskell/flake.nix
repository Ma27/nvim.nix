{
  description = "A dev environment for a Haskell project";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  inputs.utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system: {
    devShells.default = with nixpkgs.legacyPackages.${system};
      mkShell {
        packages = [
          haskellPackages.haskell-language-server
          ghc
          cabal
          hlint
        ];
      };
  });
}
