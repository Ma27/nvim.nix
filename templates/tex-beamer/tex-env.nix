# Generated with tex2nix 0.0.0
{ texlive, extraTexPackages ? {} }:
(texlive.combine ({
    inherit (texlive) scheme-small;
    "amsmath" = texlive."amsmath";
    "atbegshi" = texlive."atbegshi";
    "atveryend" = texlive."atveryend";
    "auxhook" = texlive."auxhook";
    "babel" = texlive."babel";
    "bitset" = texlive."bitset";
    "booktabs" = texlive."booktabs";
    "csquotes" = texlive."csquotes";
    "enumitem" = texlive."enumitem";
    "etexcmds" = texlive."etexcmds";
    "etoolbox" = texlive."etoolbox";
    "geometry" = texlive."geometry";
    "gettitlestring" = texlive."gettitlestring";
    "hopatch" = texlive."hopatch";
    "hycolor" = texlive."hycolor";
    "hypdoc" = texlive."hypdoc";
    "hyperref" = texlive."hyperref";
    "iftex" = texlive."iftex";
    "infwarerr" = texlive."infwarerr";
    "intcalc" = texlive."intcalc";
    "kvdefinekeys" = texlive."kvdefinekeys";
    "kvoptions" = texlive."kvoptions";
    "kvsetkeys" = texlive."kvsetkeys";
    "letltxmacro" = texlive."letltxmacro";
    "listings" = texlive."listings";
    "ltxcmds" = texlive."ltxcmds";
    "mdframed" = texlive."mdframed";
    "minitoc" = texlive."minitoc";
    "needspace" = texlive."needspace";
    "ntheorem" = texlive."ntheorem";
    "pdfescape" = texlive."pdfescape";
    "pdftexcmds" = texlive."pdftexcmds";
    "refcount" = texlive."refcount";
    "relsize" = texlive."relsize";
    "rerunfilecheck" = texlive."rerunfilecheck";
    "subfigure" = texlive."subfigure";
    "type1cm" = texlive."type1cm";
    "uniquecounter" = texlive."uniquecounter";
    "url" = texlive."url";
    "wrapfig" = texlive."wrapfig";
    "xcolor" = texlive."xcolor";
    "zref" = texlive."zref";

} // extraTexPackages))
