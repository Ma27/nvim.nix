\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ma27-beam}

\LoadClass[10pt]{beamer}

%% Math
\usepackage{amssymb}    % additional symbols
\usepackage{amsmath}    % math expressions
\usepackage{amsthm}     % newtheorem

%% Layout
\usepackage{multicol}   % Multi-column List
\usepackage{wrapfig}		% Floating layout around a figure
\usepackage{mdframed}   % Text Blocks
\usepackage{zref}

%% Encoding
\usepackage[german]{babel}
\usepackage{color} 
\usepackage[utf8]{inputenc}
\usepackage{csquotes}
\usepackage{subfigure}

%% Hyperlinks
\usepackage{hyperref}

%% Theming
\usetheme{Warsaw}
\setbeamertemplate{navigation symbols}{}
\setbeamercovered{transparent}
