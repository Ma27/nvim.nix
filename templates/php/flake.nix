{
  description = "A dev environment for a PHP project";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  inputs.utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system: {
    devShells.default = with nixpkgs.legacyPackages.${system};
      mkShell {
        packages = [
          (php82.buildEnv {
            extensions = { all, enabled }: with all; enabled ++ [ xdebug apcu ];
          })
          phpPackages.composer
          phpPackages.phpstan
          phpPackages.psysh
          phpactor
        ];
      };
  });
}
