# `nvim.nix`

[![pipeline status](https://img.shields.io/endpoint?url=https://hydra.ist.nicht-so.sexy/job/nvim-nix/master/release.x86_64-linux/shield)](https://hydra.ist.nicht-so.sexy/job/nvim-nix/master/release.x86_64-linux/)

![Preview Dark](https://wolke.mbosch.me/apps/files_sharing/publicpreview/BtiMwDnn5WyPetk?file=%2F&fileId=1241109&x=3840&y=2160&a=true&etag=6a7a04f75265a89c328e2e16c65ff46b)
![Preview Light](https://wolke.mbosch.me/apps/files_sharing/publicpreview/wZBR64E8e8yG8mZ?file=%2F&fileId=1241110&x=3840&y=2160&a=true&etag=5755064a8ab44ba1daee8dabb1d90fc4)

This repository contains some simple [Nix expressions](https://nixos.org/nix/) to build
and hack on my personal [neovim](https://neovim.io/) distribution.

I decided to move this into its own repository as development cycles with e.g. `nixos-rebuild(8)`
took way too long for proper debugging and I figured that some snippets might be
actually helpful for somebody else.

## Features

* Dark/Light colorscheme based on [the Kanagawa scheme](https://github.com/rebelot/kanagawa.nvim)
  with a few modifications.
* [LSP](https://microsoft.github.io/language-server-protocol/) setup with [cmp](https://github.com/hrsh7th/nvim-cmp)
  as completion provider, configured with the following implementations:
  * [`PLS`](https://github.com/FractalBoy/perl-language-server) (Perl)
  * [`clangd`](https://clangd.llvm.org/) (C/C++)
  * [`gopls`](https://pkg.go.dev/golang.org/x/tools/gopls)
  * [`jsonnet-language-server`](https://github.com/grafana/jsonnet-language-server)
  * [`lua-language-server`](https://github.com/LuaLS/lua-language-server)
  * [`nil`](https://github.com/oxalica/nil) (Nix)
  * [`phpactor`](https://github.com/phpactor/phpactor)
  * [`python-lsp-server`](https://github.com/python-lsp/python-lsp-server)
  * [`rustaceanvim`](https://github.com/mrcjkb/rustaceanvim)
  * [`typescript-language-server`](https://github.com/typescript-language-server/typescript-language-server)
  * [`vscode-langservers-extracted`](https://github.com/hrsh7th/vscode-langservers-extracted) (HTML/CSS, Markdown)
* [git](https://git-scm.com/)-Integration with [gitsigns](https://github.com/lewis6991/gitsigns.nvim)
  to display per-line changes of files and
  [fugivite.vim](https://github.com/tpope/vim-fugitive) for commits and git-blame.
* [vimtex](https://github.com/lervag/vimtex) configuration for notes-taking.
* Custom quit helper on `:q`. Buffers are used for multiple opened files, but `:q` behaves as if we'd have tabs, i.e.
  * `:q` on the last buffer quits vim entirely.
  * `:q` on `nvim-tree` closes the filebrowser everywhere via its API.
  * `:q` on a buffer (that's not the last) deletes the current buffer.
  * The same applies to `:wq` and `:q!`
* Snippet library based on [UltiSnips](https://github.com/SirVer/ultisnips).
  * Additional snippets can be placed in `NVIM_NIX_EXTRA_SNIPPETS_DIR`.
* Markdown preview using [`grip`](https://github.com/joeyespo/grip).
* [`whichkey`](https://github.com/folke/which-key.nvim) helps remembering which key combinations exist.
* An undofile is kept in `$XDG_CACHE_HOME/nvim-undodir` to allow undo/redo operations on files.
  Additionally all change records are kept in a tree using [`mundo`](https://github.com/simnalamburt/vim-mundo).
* Optional filebrowser via [`nvim-tree`](https://github.com/nvim-tree/nvim-tree.lua).

## Installation

This setup can be installed into [`home-manager`](https://github.com/nix-community/home-manager) like this:

```nix
{
  imports = [ inputs.nvim-nix.hmModules.myvim ];
  ma27.home.nvim.enable = true;
}
```

* By default, the terminal cursor will be reset from a block (as in *normal* mode) to a slim
  one (*Beam*). This behavior can be turned off with `ma27.home.nvim.vimBeam = false;`.
* By default, `EDITOR` will be set to `vim` and `GIT_EDITOR` to `vim +start` (to automatically
  switch to *insert* mode). This behavior can be turned off with
  `ma27.home.nvim.defaultEditor = false;`

__Note:__ you'll need [fira code](https://github.com/tonsky/FiraCode) as icon font installed.
Fonts can be installed like this:

```nix
{ pkgs, ... }: {
  fonts = {
    fontDir.enable = true;
    enableGhostscriptFonts = true;

    fonts = with pkgs; [
      roboto
      (nerdfonts.override { fonts = [ "FiraCode" ]; })
      fira-code
    ];
  };
}
```

...and used in `foot` like this:

```nix
{
  programs.foot.settings.main.font = lib.concatMapStringsSep ", " (x: "${x}:size=11") [
    "Fira Code"
    "FiraCode Nerd Font"
    "Noto Color Emoji"
  ];
}
```

## Custom shortcuts

| Shortcut | Explanation | Mode |
| --- | --- | --- |
| `\` | `<localleader>` | any |
| `<F2>` | Open nvim-tree on the left | normal, insert, visual |
| `<F3>` | Open nvim-tree on the left and jump to currently open file | normal, insert, visual |
| `<F5>` | Show do/undo tree of current file | any |
| `<F6>` | Start/Stop markdown preview of current file | normal, insert |
| `<F7>` | List currently active markdown previews | normal, insert |
| `<F8>` | Show list of all available bindings with WhichKey | normal, insert, visual |
| `<F9>` | Toggle fold on the block below the curser | normal |
| `<C-p>` | Telescope search to find files in current directory | normal |
| `<C-b>` | Telescope search to find open vim buffers | normal |
| `<C-s>` | Telescope search to find available snippets | normal |
| `<C-f>` | Telescope search to grep through files in current directory | normal |
| `<C-,>` | Telescope search for warnings/errors from LSP | normal |
| `<localleader>s` | Search through git's stash | normal |
| `<localleader>b` | Show git blame for current file | normal |
| `<C-g>` | Insert empty hash for Nix FoD | insert (Nix only) |
| `<C-n>` | Create a new LaTeX document and include it via \\input into the current buffer | insert (TeX only) |
| `<localleader>gs` | Stage hunk | normal |
| `<localleader>gr` | Undo stage hunk | normal |
| `<localleader>gU` | Reset buffer | normal |
| `<localleader>gu` | Reset hunk | normal |
| `]h` | Jump to next hunk | normal |
| `[h` | Jump to previous hunk | normal |
| `K` | Hover over current symbol for docs (lsp.buf.hover) | normal (LSP) |
| `gD` | Go to implementation of the current symbol (lsp.buf.implementation) | normal (LSP) |
| `<C-K>` | Show signature of current symbol (lsp.buf.signature_help) | normal (LSP) |
| `1gD` | Show type of the current symbol (lsp.buf.type_definition) | normal (LSP) |
| `gr` | List references of current symbol (lsp.buf.references) | normal (LSP) |
| `gR` | Rename current symbol (lsp.buf.rename) | normal (LSP) |
| `g0` | List all symbols in quickfix window (lsp.buf.document_symbol) | normal (LSP) |
| `gW` | List all symbols of the workspace in quickfix window (lsp.buf.workspace_symbol) | normal (LSP) |
| `gd` | Jumps to the declaration of the current symbol (lsp.buf.declaration) | normal (LSP) |
| `gF` | Format current buffer (lsp.buf.format) | normal (LSP) |
| `ca` | Execute given code action from current LSP (lsp.buf.code_action) | normal (LSP) |
| `<C-]>` | Jumps to the definition of the current symbol, mostly preferable over unimplemented declaration (lsp.buf.definition) | normal (LSP) |
| `g,` | Explains the error below the cursor in more detail | normal (rustaceanvim) |
| `g.` | Opens docs for the symbol below the cursor | normal (rustaceanvim) |
| `<localleader>e` | Toggle lsp_lines | normal |

## Templates

Templates are useful to quickly initialize dev shells with tooling, compilers and LSPs. Assuming
the local registry contains this flake, e.g. via

```nix
{
  nix.registry.nvim-nix.flake = inputs.nvim-nix;
}
```

Then e.g. a dev shell for Rust development with formatter, compiler and `rust-analyzer` (including `.envrc`)
can be initialized like this:

```
nix flake init -t nvim-nix#rust
```

Use `nix flake show` to find out which templates are currently available.

## Patches

The following patches are used for vim plugins:

| Plugin | Description |
| --- | --- |
| `nvim-autopairs` | `<token><cr>` with token being e.g. a bracket expands the expression over three lines (one for opening and closing bracket, one for the cursor). This behavior is **disabled** if the token is a double quote. |
| `fugitive` | Also show the commit hash (not only the tree hash) in the diff window |
| `lualine` | Also show total length of a file in numbers at the bottom right |
| `vim-nix` | Don't reindent the current line when entering a newline with `<CR>` |
| `telescope` | Include everything **but** `.git` when searching for files |
