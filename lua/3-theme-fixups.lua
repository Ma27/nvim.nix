local groupsToLink = {
  -- link all booleans to the same group
  nixBoolean = 'Boolean',

  -- consistent coloring for includes etc.
  Include = 'NixVimLinks',
  Directory = 'NixVimLinks',

  -- nix: increase readability for various expression types
  nixPath = 'NixVimLinks',
  nixAttributeDot = 'Operator',
  nixNull = '@boolean',
  nixParen = 'Operator',
  nixAttributeSet = 'Structure',
  nixListBracket = 'Structure',

  nixInterpolationDelimiter = '@lsp.type.enum',

  ['@lsp.type.variable.nix'] = '@lsp.type.method',
}

local groupsWithFgColor = {
  -- links (from https://material-theme.com/docs/reference/color-palette/)
  NixVimLinks = '#80cbc4',
}

for group, linkTarget in pairs(groupsToLink) do
  vim.api.nvim_set_hl(0, group, {})
  vim.api.nvim_set_hl(0, group, {
    link = linkTarget,
    nocombine = true,
  })
end

for group, guifg in pairs(groupsWithFgColor) do
  vim.api.nvim_set_hl(0, group, {})
  vim.api.nvim_set_hl(0, group, {
    fg = guifg,
  })
end
