-- notification / completion bar
require("noice").setup()

-- completion / lsp
local cmp = require('cmp')
cmp.setup({
  snippet = {
    expand = function(args) vim.fn["UltiSnips#Anon"](args.body) end
  },
  sources = cmp.config.sources({
    { name = 'buffer' },
    { name = 'emoji' },
    { name = 'greek' },
    { name = 'nvim_lsp' },
    { name = 'nvim_lua' },
    { name = 'path' },
    { name = 'ultisnips' },
  }),
  mapping = {
    ['<CR>'] = cmp.mapping.confirm({ select = false }),
    ['<Down>'] = cmp.mapping(cmp.mapping.select_next_item({
      behavior = cmp.SelectBehavior.Select
    }), {'i', 'c'}),
    ['<Up>'] = cmp.mapping(cmp.mapping.select_prev_item({
      behavior = cmp.SelectBehavior.Select
    }), {'i', 'c'}),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
  },
  formatting = {
    fields = {'menu', 'abbr', 'kind'},
    format = function(entry, item)
      local menu_icon = {
        nvim_lsp = 'λ',
        buffer = 'Ω',
        path = '',
        emoji = '',
      }

      item.menu = menu_icon[entry.source.name]
      return item
    end,
  },
})

local cmdline_mapping = cmp.mapping.preset.cmdline({
  ['<CR>'] = cmp.mapping.confirm({ select = false }),
})
cmdline_mapping['<Down>'] = cmdline_mapping['<Tab>']
cmdline_mapping['<Up>'] = cmdline_mapping['<S-Tab>']
cmp.setup.cmdline({ '/', '?' }, {
  mapping = cmdline_mapping,
  sources = {
    { name = 'buffer' },
  }
})
local described_opts = function (bufnr, desc)
  return vim.g.nvimnix.keymap.add_desc(
    vim.g.nvimnix.keymap.with_bufnr(bufnr),
    desc
  )
end
local lsp_on_attach = function (_, bufnr)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, described_opts(
    bufnr, 'Hover over current symbol for docs (lsp.buf.hover)'
  ))
  vim.keymap.set('n', 'gD', vim.lsp.buf.implementation, described_opts(
    bufnr, 'Go to implementation of the current symbol (lsp.buf.implementation)'
  ))
  vim.keymap.set('n', '<c-k>', vim.lsp.buf.signature_help, described_opts(
    bufnr, 'Show signature of current symbol (lsp.buf.signature_help)'
  ))
  vim.keymap.set('n', '1gD', vim.lsp.buf.type_definition, described_opts(
    bufnr, 'Show type of the current symbol (lsp.buf.type_definition)'
  ))
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, described_opts(
    bufnr, 'List references of current symbol (lsp.buf.references)'
  ))
  vim.keymap.set('n', 'gR', vim.lsp.buf.rename, described_opts(
    bufnr, 'Rename current symbol (lsp.buf.rename)'
  ))
  vim.keymap.set('n', 'g0', vim.lsp.buf.document_symbol, described_opts(
    bufnr, 'List all symbols in quickfix window (lsp.buf.document_symbol)'
  ))
  vim.keymap.set('n', 'gW', vim.lsp.buf.workspace_symbol, described_opts(
    bufnr, 'List all symbols of the workspace in quickfix window (lsp.buf.workspace_symbol)'
  ))
  vim.keymap.set('n', 'gd', vim.lsp.buf.declaration, described_opts(
    bufnr, 'Jumps to the declaration of the current symbol (lsp.buf.declaration)'
  ))
  vim.keymap.set('n', 'gF', vim.lsp.buf.format, described_opts(
    bufnr, 'Format current buffer (lsp.buf.format)'
  ))
  vim.keymap.set('n', 'ca', vim.lsp.buf.code_action, described_opts(
    bufnr, 'Execute given code action from current LSP (lsp.buf.code_action)'
  ))
  vim.keymap.set(
    'n',
    '<c-]>',
    function ()
      vim.lsp.buf.definition({
        reuse_win = true,
      })
    end,
    described_opts(
      bufnr,
      'Jumps to the definition of the current symbol, mostly preferable over '
      .. 'unimplemented declaration (lsp.buf.definition)'
    )
  )
end
local lsp_srcs = { "cssls", "gopls", "hls", "html", "jsonls", "jsonnet_ls", "ts_ls", "gitlab_ci_ls", "yamlls" }
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
for _, lsp in ipairs(lsp_srcs) do
  require('lspconfig')[lsp].setup {
    on_attach = lsp_on_attach,
    capabilities = capabilities,
  }
end
require('lspconfig')['nixd'].setup {
  on_attach = lsp_on_attach,
  on_new_config = function(config, root_dir)
     local actual_root = require('lspconfig').util.find_git_ancestor(root_dir)
     if actual_root ~= nil then
       local base = vim.fs.basename(actual_root)
       if vim.startswith(base, "nixpkgs") then
         config.settings.nixd.nixpkgs.expr = "import " .. actual_root .. " {}"
         return
       end
     end

     local nixpkgs_path = os.getenv('NVIM_NIX_NIXD_NIXPKGS')
     if nixpkgs_path ~= nil then
       config.settings.nixd.nixpkgs.expr = "import " .. nixpkgs_path .. " {}"
     end
  end,
  settings = {
    nixd = {
      nixpkgs = {
        expr = 'import <nixpkgs> {}'
      },
      --options = {
        --nixos = {
          ---- Desired behavior:
          ---- * options from env expr?
          ---- * nixpkgs/nixos
          --expr = "(builtins.getFlake \"/home/ma27/Projects/infra\").nixosConfigurations.snens.options",
        --}
      --},
    },
  },
}

local capabilities_semantic_tokens_only = {
  textDocument = {
    semanticTokens = capabilities.textDocument.semanticTokens,
  }
}
require('lspconfig')['nil_ls'].setup {
  capabilities = capabilities_semantic_tokens_only,
}
require('lspconfig')['phpactor'].setup {
  on_attach = lsp_on_attach,
  capabilities = capabilities,
  -- super-ugly workaround
  -- `lualine.nvim/lua/lualine/utils/nvim_opts.lua:77: E539: Illegal character <,>`
  -- which is because of phpactor's progress bar (the `-v` means verbose and
  -- lists all files on a single line rather than the bar)
  cmd = {'phpactor', 'language-server', '-v'}
}
local util = require 'lspconfig/util'
require('lspconfig').clangd.setup {
  root_dir = util.root_pattern('compile_commands.json', '.git'),
  capabilities = capabilities,
  on_attach = lsp_on_attach,
  cmd = {'clangd', '-j', '8'}
}

local function get_python_path()
  return vim.fn.exepath("python")
end
require('lspconfig').pylsp.setup {
  on_new_config = function(config)
    config.settings.pylsp.plugins.jedi.environment = get_python_path(config.root_dir)
    config.settings.pylsp_mypy.overrides = {"--python-executable", get_python_path(config.root_dir), true}
  end,
  on_attach = lsp_on_attach,
  capabilities = capabilities,
  settings = {
    pylsp = {
      plugins = {
        jedi = {
          environment = nil,
        },
        ruff = {
          enabled = true,
        }
      }
    }
  }
}
require('lspconfig').perlpls.setup {
  capabilities = capabilities,
  on_attach = lsp_on_attach,
  settings = {
    perl = {
      perlcritic = {
        enabled = true,
      },
    },
  },
}
require('lspconfig').lua_ls.setup {
  capabilities = capabilities,
  on_attach = lsp_on_attach,
  settings = {
    Lua = {
      runtime = {
        version = 'LuaJIT',
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = {'vim'},
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
      },
      telemetry = {
        enable = false,
      },
    }
  }
}

require("lsp_lines").setup()
vim.diagnostic.config({
  virtual_text = false,
})
vim.keymap.set('n', '<localleader>e', function ()
  require("lsp_lines").toggle()
end, vim.g.nvimnix.keymap.with_desc(
  'Toggle lsp_lines'
))

-- rust integration
vim.g.rustaceanvim = {
  server = {
    on_attach = function (x, bufnr)
      lsp_on_attach(x, bufnr)


      for code, cfg in pairs({
        ['g,'] = {'explainError', 'Open an overlay with a more detailed error explanation'},
        ['g.'] = {'openDocs', 'Open docs for the symbol below cursor'}
      }) do
        vim.keymap.set(
          'n',
          code,
          function () vim.cmd.RustLsp(cfg[1]) end,
          described_opts(
            bufnr,
            cfg[2]
          )
        )
      end
    end,
  }
}

-- lsp gimmicks
require'nvim-treesitter.configs'.setup {
  auto_install = false,
  highlight = {
    enable = true,
    disable = {"markdown"},
    additional_vim_regex_highlighting = true,
  }
}

require('illuminate').configure({
  providers = {
    'lsp'
  },
  delay = 100,
  under_cursor = true,
})

-- indent guides
require('ibl').setup {
  indent = {
    char = {'│'}
  },
}

-- autoclose brackets, quotes etc.
local npairs = require("nvim-autopairs")
local Rule = require('nvim-autopairs.rule')
local cond = require('nvim-autopairs.conds')
npairs.setup{}
npairs.add_rule(Rule("''", "''", "nix")
  :with_pair(cond.not_after_regex('"'))
  :with_pair(cond.not_before_regex([["']], 2)))
npairs.add_rule(Rule("<!--", "-->", "markdown"))
npairs.remove_rule("'")
npairs.add_rule(Rule("'", "'"):with_pair(cond.not_filetypes({"nix","haskell"})))

-- telescope
local telescope = require('telescope')
local actions = require("telescope.actions")
telescope.setup {
  defaults = {
    layout_strategy = 'bottom_pane',
    mappings = {
      i = {
        ["<esc>"] = actions.close,
      },
    },
  },
}

telescope.load_extension('ultisnips')
telescope.load_extension('lsp_handlers')
telescope.load_extension('ui-select')
telescope.load_extension('noice')

vim.keymap.set({'n', 'i'}, '<C-h>', ':Noice telescope<CR>', vim.g.nvimnix.keymap.with_desc(
  'History of all messages'
))
vim.keymap.set('n', '<C-p>', ':Telescope find_files hidden=true<CR>', vim.g.nvimnix.keymap.with_desc(
  'Telescope search to find files in current directory'
))
vim.keymap.set('n', '<C-b>', ':Telescope buffers<CR>', vim.g.nvimnix.keymap.with_desc(
  'Telescope search to find open vim buffers'
))
vim.keymap.set('n', '<C-s>', ':Telescope ultisnips<CR>', vim.g.nvimnix.keymap.with_desc(
  'Telescope search to find available snippets'
))
vim.keymap.set('n', '<C-f>', ':Telescope live_grep<CR>', vim.g.nvimnix.keymap.with_desc(
  'Telescope search to grep through files in current directory'
))
vim.keymap.set('n', '<C-,>', ':Telescope diagnostics<CR>', vim.g.nvimnix.keymap.with_desc(
  'Telescope search for warnings/errors from LSP'
))
vim.keymap.set('n', '<localleader>s', ':Telescope git_stash<CR>', vim.g.nvimnix.keymap.with_desc(
  'Telescope search to browse through current git stash'
))

-- git integration
require('gitsigns').setup {
  current_line_blame = true,
  on_attach = function (bufnr)
    local opts = vim.g.nvimnix.keymap.with_bufnr(bufnr)
    vim.keymap.set('n', '<Leader>gs', ':Gitsigns stage_hunk<CR>', opts)
    vim.keymap.set('n', '<Leader>gr', ':Gitsigns undo_stage_hunk<CR>', opts)
    vim.keymap.set('n', '<Leader>gU', ':Gitsigns reset_buffer<CR>', opts)
    vim.keymap.set('n', '<Leader>gu', ':Gitsigns reset_hunk<CR>', opts)

    local gs = package.loaded.gitsigns
    local nav_opts = opts
    nav_opts.expr = true
    for code, fn in pairs({[']h'] = gs.next_hunk, ['[h'] = gs.prev_hunk}) do
      vim.keymap.set('n', code, function ()
        if vim.wo.diff then return code end
        vim.schedule(function() fn() end)
        return '<Ignore>'
      end, nav_opts)
    end
  end,
}

vim.keymap.set('n', '<localleader>b', '<cmd>Git blame<CR>', vim.g.nvimnix.keymap.with_desc(
  'Show git blame for current file'
))

-- nicer icons
require('nvim-web-devicons').setup {
  default = true
}

-- status lines
require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'kanagawa',
  },
  sections = {
    lualine_x = {
      'encoding',
      'fileformat',
      'filetype',
      'lsp_progress'
    }
  }
}

-- top-line, buffers
local bufferline = require('bufferline')
bufferline.setup {}

local mk_buf_helper = function (code, fk, action, direction)
  if fk ~= '' then
    fk = fk .. '-'
  end
  vim.keymap.set(
    {'n', 'v', 'i'},
    '<' .. fk .. code .. '>',
    function ()
      vim.cmd('BufferLine' .. action .. direction)
    end,
    vim.g.nvimnix.keymap.with_desc(
      action .. ' in buffer list to the ' .. direction .. ' file'
    )
  )
end

for code, action in pairs({PageUp = 'Prev', PageDown = 'Next'}) do
  mk_buf_helper(code, 'C', 'Cycle', action)
  mk_buf_helper(code, 'C-S', 'Move', action)
end

-- filebrowser
require('nvim-tree').setup()
vim.keymap.set({'n', 'i', 'v'}, '<F2>', ':NvimTreeToggle<CR>', vim.g.nvimnix.keymap.with_desc(
  'Open nvim-tree on the left'
))
vim.keymap.set({'n', 'i', 'v'}, '<F3>', ':NvimTreeFindFile<CR>', vim.g.nvimnix.keymap.with_desc(
  'Open nvim-tree on the left and jump to currently open file'
))

-- ultisnips
vim.g.UltiSnipsJumpForwardTrigger = '<c-b>'
vim.g.UltiSnipsJumpBackwardTrigger = '<c-z>'

-- closetags
vim.g.closetag_filenames = table.concat({
  '*.html',
  '*.js',
  '*.jsx',
  '*.phtml',
  '*.tt',
  '*.xhtml',
  '*.xml',
}, ',')

-- mundo (undo-tree)
vim.keymap.set('', '<F5>', '<cmd>MundoToggle<CR>', vim.g.nvimnix.keymap.with_desc(
  'Show do/undo tree of current file'
))

-- jsonnet autoformat disable
vim.g.jsonnet_on_save = false

-- no additional spell checking for markdown
vim.g.markdown_enable_spell_checking = false

-- multi-cursor
vim.g.VM_mouse_mappings = 1
vim.g.VM_maps = {
  ['Undo'] = 'u',
  ['Redo'] = '<C-r>',
}

-- go back to the last cursor position when re-opening a file
require'nvim-lastplace'.setup{}

-- open URLs in the browser
require('gx').setup{}

-- markdown preview
vim.g.grip_default_map = false
vim.keymap.set(
  {'n', 'i'},
  '<F6>',
  function ()
    if vim.fn.exists(':GripStop') > 0 then
      vim.cmd(':GripStop')
      vim.api.nvim_err_writeln("Stopped already active markdown preview")
    else
      vim.cmd(':GripStart')
    end
  end,
  vim.g.nvimnix.keymap.with_desc('Start/Stop markdown preview of current file')
)

vim.keymap.set(
  {'n', 'i'},
  '<F7>',
  function ()
    if vim.fn.exists(':GripList') > 0 then
      vim.cmd(':GripList')
    else
      vim.api.nvim_err_writeln("No active previews")
    end
  end,
  vim.g.nvimnix.keymap.with_desc('List currently active markdown previews')
)

-- which shortcuts are even available?
require("which-key").setup {}
vim.o.timeout = true
vim.o.timeoutlen = 300
vim.keymap.set(
  {'n', 'i', 'v'},
  '<F8>',
  function ()
    vim.cmd('WhichKey')
  end,
  vim.g.nvimnix.keymap.with_desc('Show list of all available bindings with WhichKey')
)

-- for gitlab-ci-ls, make these files special
for _, ptrn in ipairs({"*.gitlab-ci*.{yml,yaml}", "templates/**.yml"}) do
  vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
    pattern = ptrn,
    callback = function()
      vim.bo.filetype = "yaml.gitlab"
    end,
  })
end
