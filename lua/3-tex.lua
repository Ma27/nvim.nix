-- tex
if vim.fn.executable('latexmk') ~= 1 then
  vim.g.vimtex_enabled = 0
end
vim.g.tex_flavor = 'latex'
vim.g.vimtex_view_method = 'zathura'
vim.g.vimtex_quickfix_mode = 0
vim.g.tex_conceal = 'abdmg'
vim.g.vimtex_compiler_progname = 'nvr'
vim.g.vimtex_use_temp_files = true

-- autopairs ($ -> $$)
local Rule = require('nvim-autopairs.rule')
require('nvim-autopairs').add_rule(Rule("$", "$", "tex"))

-- helper to create a new file for the current document
vim.api.nvim_create_autocmd('FileType', {
  pattern = {'tex'},
  group = 'CustomGeneralHelpers',
  callback = function (opts)
    vim.keymap.set(
      'i',
      '<C-n>',
      function()
        local function dirname(str)
          if str:match(".-/.-") then
            local name = string.gsub(str, "(.*/)(.*)", "%1")
            return name
          else
            return ''
          end
        end
        local path = vim.fn.input('Enter path: ')
        if (path == '' or path == nil) then
          print(" ") -- clear input line before printing an error
          vim.api.nvim_err_writeln("No target path specified!")
        else
          local fullpath = string.format('%s/%s', vim.fn.expand('%:p:h'), path)
          if vim.fn.filereadable(fullpath) == 1 then
            print(" ") -- clear input line before printing an error
            vim.api.nvim_err_writeln(string.format("Note '%s' already exists", fullpath))
          else
            vim.fn.mkdir(dirname(fullpath), 'p')
            vim.fn.setline(
              vim.fn.line('.'),
              string.format(
                "%s\\input{%s}",
                vim.fn.getline('.'),
                path
              )
            )
            vim.api.nvim_command('w | redraw!')
            vim.api.nvim_command(string.format('tabnew %s', fullpath))
          end
        end
      end,
      vim.g.nvimnix.keymap.with_bufnr_and_desc(
        opts.buf,
        'Create a new LaTeX document and include it via \\input into the current buffer'
      )
    )
  end,
})
