local function cache_dir()
  local dir = os.getenv('XDG_CACHE_HOME')
  if dir ~= nil then
    return dir
  end
  return string.format('%s/.cache', os.getenv('HOME'))
end

-- custom namespace with my own additions
vim.g.nvimnix = {
  xdg_cache_dir = cache_dir(),
  keymap = {
    options = {
      noremap = true,
      silent = true,
    },
    with_desc = function (desc)
      local new = vim.g.nvimnix.keymap.options
      new['desc'] = desc
      return new
    end,
    add_desc = function (opts, desc)
      opts['desc'] = desc
      return opts
    end,
    with_bufnr = function(bufnr)
      local new = vim.g.nvimnix.keymap.options
      new['buffer'] = bufnr
      return new
    end,
    with_bufnr_and_desc = function (bufnr, desc)
      return vim.g.nvimnix.keymap.add_desc(
        vim.g.nvimnix.keymap.with_bufnr(bufnr),
        desc
      )
    end,
  },
  indent_settings = {
    default = 2,
    bufs_using_tabs = {
      'go',
      'make'
    },
    -- NOTE: these are just sensible defaults: in the end, sleuth
    -- automatically detects what to do, but by default python has 4 spaces
    -- etc.
    bufs_custom_indent = {
      c = 4,
      cpp = 4,
      docbk = 1,
      java = 4,
      perl = 4,
      php = 4,
      py = 4,
    },
  }
}
