vim.opt.expandtab = true  -- In Insert mode: Use the appropriate number of spaces to insert a <Tab>
                          -- (<C-V><Tab> is a tab in insert mode).
vim.opt.smarttab = true   -- A <Tab> in front of a line inserts blanks according to 'shiftwidth'.
vim.opt.autoindent = true -- Copy indent from current line when starting a new line.

vim.opt.tabstop = 2       -- A tab equals two spaces (relevant for viewing e.g. Makefiles).
vim.opt.shiftwidth = 2    -- On autoindent (or manual indent via >) use _2_ spaces.

-- customizations for different filetypes
vim.api.nvim_create_augroup('CustomFileIndentSettings', { clear = true })

vim.api.nvim_create_autocmd('FileType', {
  group = 'CustomFileIndentSettings',
  pattern = vim.g.nvimnix.indent_settings.bufs_using_tabs,
  callback = function ()
    vim.opt.expandtab = false
  end,
})

for lang, indent in pairs(vim.g.nvimnix.indent_settings.bufs_custom_indent) do
  vim.api.nvim_create_autocmd('FileType', {
    group = 'CustomFileIndentSettings',
    pattern = {lang},
    callback = function ()
      vim.opt_local.shiftwidth = indent
      vim.opt_local.tabstop = indent
    end,
  })
end
