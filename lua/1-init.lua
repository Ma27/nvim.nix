vim.opt.encoding = 'UTF-8'

-- colorscheme
local kanagawa = require("kanagawa")
kanagawa.setup({})
kanagawa.load("wave")

-- general editor settings
vim.opt.background = 'dark'
vim.opt.mouse = 'a'
vim.opt.number = true
vim.opt.scrolloff = 3
vim.opt.termguicolors = true
vim.opt.conceallevel = 1      -- to show special symbols, e.g. in latex

vim.opt.fillchars['vert'] = '│'
vim.opt.list = true
vim.opt.listchars = { tab = '→ ', trail = '·', precedes = '←', extends = '→' }

vim.opt.backspace = { 'indent', 'eol', 'start' }  -- Delete like in most other programs.

-- cursorline, to highlight the current focused line
vim.opt.cursorline = true

-- clipboard writes into system clipboard
vim.opt.clipboard = 'unnamedplus'

-- persistent undo (to `~/.cache/nvim-undodir`)
vim.opt.undofile = true
vim.opt.undodir = string.format('%s/nvim-undodir', vim.g.nvimnix.xdg_cache_dir)

-- highlight-search
vim.opt.hlsearch = true
vim.keymap.set('n', "<localleader>'", '<cmd>nohl<CR>', vim.g.nvimnix.keymap.with_desc(
  'Reset highlighted search results'
))

-- folding
vim.opt.foldmethod = 'indent'
vim.opt.foldcolumn = '0'
vim.opt.foldlevel = 99
vim.keymap.set('n', '<F9>', 'za', vim.g.nvimnix.keymap.with_desc(
  'Toggle fold on the block below the curser'
))

-- speling
vim.opt.spell = true
vim.opt.spelllang = 'de,en_us'

-- allow pressing > multiple times when indenting in visual mode
vim.keymap.set('x', '<', '<gv', vim.g.nvimnix.keymap.options)
vim.keymap.set('x', '>', '>gv|', vim.g.nvimnix.keymap.options)

-- a few filetype customizations
vim.filetype.add({
  extension = {
    tt = 'tt2html',
    prc = 'sql',
  },
})

-- custom keymaps for helper bindings
vim.api.nvim_create_augroup('CustomGeneralHelpers', { clear = true })
vim.api.nvim_create_autocmd('FileType', {
  pattern = {'nix'},
  group = 'CustomGeneralHelpers',
  callback = function (opts)
    vim.keymap.set(
      'i',
      '<C-g>',
      'hash = "sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";',
      vim.g.nvimnix.keymap.with_bufnr_and_desc(opts.buf, 'Insert empty hash for Nix FoD')
    )
  end,
})

-- with conceallevel>0, the json extension hides the quotes which
-- looks odd.
vim.api.nvim_create_autocmd('FileType', {
  group = 'CustomGeneralHelpers',
  pattern = {'json'},
  callback = function ()
    vim.opt_local.conceallevel = 0
  end
})

-- quit just as we'd have tabs here: override `q` etc. in command mode
-- and check if >1 buffer is there: if yes, then delete the current buffer,
-- otherwise quit vim entirely.
local mk_custom_quit = function (do_save, force, abb)
  local do_force = function (cmd)
    if force then
      return cmd .. '!'
    end
    return cmd
  end
  local last_win_is_nvim_tree = function ()
    return #vim.api.nvim_list_wins() == 1 and vim.api.nvim_buf_get_name(0):match("NvimTree_") ~= nil
  end

  return function ()
    local type = vim.fn.getcmdtype()
    local current_cmdline = vim.fn.getcmdline()
    if type ~= ':' or current_cmdline ~= '' then
      vim.fn.setcmdline(current_cmdline .. abb)
      return
    end
    if do_save then
      vim.cmd.w()
    end
    if #vim.fn.getbufinfo({buflisted=1}) == 1 then
      -- last open buffer -> we quit vim
      vim.cmd(do_force('quit'))
      -- if a filebrowser is remaining, also perform a quit.
      if last_win_is_nvim_tree() then
        vim.cmd(do_force('quit'))
      end
    elseif vim.api.nvim_buf_get_name(0):match("NvimTree_") ~= nil then
      -- not last open window and we want to close nvim-tree -> close it everywhere
      require('nvim-tree.api').tree.close()
      -- leave cmdline. Otherwise we'll stay in the mode and also not
      -- have the buffer closed.
      vim.api.nvim_input('<ESC>')
    else
      -- a random open buffer, but we have >1 buffer -> just delete the buffer
      vim.cmd(do_force('bdelete'))
      -- is the last currently visible window the filebrowser?
      if last_win_is_nvim_tree() then
        -- also delete this buffer. Then the last used buffer (including file-browser)
        -- will be opened.
        --
        -- This will be triggered in the following case
        -- * one buffer with $file is open
        -- * <F2> is pressed for the file-browser
        -- * a new file is opened in another buffer (left is the browser and right/main is the
        --   buffer. When closing this buffer, we also need to kill the buffer with the filebrowser,
        --   otherwise we'll only see the file-browser without an open file).
        vim.cmd(do_force('bdelete'))
      end
      -- leave cmdline. Otherwise we'll stay in the mode and also not
      -- have the buffer closed.
      vim.api.nvim_input('<ESC>')
    end
  end
end

local noremap = { noremap = true }

vim.keymap.set('c', 'q<cr>', mk_custom_quit(false, false, 'q'), noremap)  -- :q
vim.keymap.set('c', 'wq<cr>', mk_custom_quit(true, false, 'wq'), noremap) -- :wq
vim.keymap.set('c', 'q!<cr>', mk_custom_quit(false, true, 'q!'), noremap) -- :q!

-- Inlays, for e.g. nixd
vim.lsp.inlay_hint.enable(true)
vim.keymap.set('n', '<Leader>L', function ()
  vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
end)
