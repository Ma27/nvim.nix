{
  description = "Opinionated NeoVim distribution built with Nix";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    rust-overlay.inputs = {
      nixpkgs.follows = "nixpkgs";
    };

    nixd = {
      url = "github:nix-community/nixd";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-parts.follows = "flake-parts";
      };
    };

    nvim-lspconfig-src = {
      url = "github:neovim/nvim-lspconfig";
      flake = false;
    };
    vim-jq-src = {
      url = "github:bfrg/vim-jq";
      flake = false;
    };
    vim-markdown-src = {
      url = "github:gabrielelana/vim-markdown";
      flake = false;
    };
    gx-nvim-src = {
      url = "github:chrishrb/gx.nvim";
      flake = false;
    };
    vim-grip-src = {
      url = "github:PratikBhusal/vim-grip";
      flake = false;
    };

    pre-commit = {
      url = "github:cachix/git-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
        gitignore.follows = "gitignore";
      };
    };

    flake-compat.url = "github:edolstra/flake-compat";
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
    };
  };

  outputs = { self, nixpkgs, nixd
            # vim plugins not packaged in nixpkgs
            , nvim-lspconfig-src, vim-jq-src, vim-markdown-src
            , gx-nvim-src, vim-grip-src
            , pre-commit
            , ... }:
  let
    inherit (nixpkgs) lib;
    forEachSystem = lib.genAttrs [ "x86_64-linux" "aarch64-darwin" ];
    forEachHydraSystem = forEachSystem;
    addMainProgram = mainProgram: pkg: pkg.overrideAttrs ({ meta ? {}, ... }: {
      meta = meta // {
        inherit mainProgram;
      };
    });
  in {
    overlays.default = final: prev: {
      myvim = addMainProgram "vim" (final.neovim.override {
        vimAlias = true;
        viAlias = true;

        withPython3 = true;

        extraMakeWrapperArgs = final.lib.escapeShellArgs [
          "--prefix"
          "PATH"
          ":"
          (final.lib.makeBinPath [
            final.neovim-remote                # for e.g. vimtex
            final.nixd                         # I want Nix LSP everywhere
            final.nil                          # Only for semantic tokens
            final.python3Packages.grip         # markdown previews
            final.vscode-langservers-extracted # html/css
            final.yaml-language-server
            final.gitlab-ci-ls
          ])
        ];

        configure = {
          customRC = let
            sourceLuaCfg = lib.foldlAttrs
              (acc: x: type: assert type == "regular";
                acc ++ lib.optional (builtins.match ".*\\.lua$" x != null) "source ${./lua/${x}}")
              [ ]
              (builtins.readDir ./lua);
            sourceUltiSnipsDirs = ''
              let g:UltiSnipsSnippetDirectories=['${final.ultisnipsDir}']
              if getenv('NVIM_NIX_EXTRA_SNIPPETS_DIR') != v:null
                call add(g:UltiSnipsSnippetDirectories, getenv('NVIM_NIX_EXTRA_SNIPPETS_DIR'))
              endif
            '';
          in final.lib.concatStringsSep
            "\n"
            (sourceLuaCfg ++ [ sourceUltiSnipsDirs ]);

          packages.myvim-with-plugins.start = with final.vimPlugins; [
            # git integration (show diff, go to hunk, blame)
            gitsigns-nvim
            vim-fugitive # only for `git blame` used currently.

            # LSP integration
            nvim-lspconfig
            lualine-lsp-progress
            lsp_lines-nvim
            rustaceanvim

            # top/bottom bar
            bufferline-nvim
            lualine-nvim

            # completion engine (also fed by LSP) + various plugins
            nvim-cmp
            cmp-buffer
            cmp-emoji
            cmp-greek
            cmp-nvim-lsp
            cmp-nvim-ultisnips
            cmp-path

            # misc helpful things
            direnv-vim
            increment-activator
            gx-nvim
            nerdcommenter
            nix-develop-nvim
            nvim-autopairs        # auto-close quotes/brackets/...
            nvim-lastplace
            ultisnips
            vim-closetag          # automatically close xml/html tags
            vim-css-color         # colorize hex color codes
            vim-grip              # markdown preview
            vim-illuminate        # highlight related tokens (e.g. references from lsp)
            vim-matchup           # highlight if/endif pairs like brackets
            vim-mundo
            vim-sleuth            # heuristics for auto-indent
            vim-visual-multi
            vimtex
            which-key-nvim        # which shortcuts are available given an input

            # Treesitter
            (nvim-treesitter.withPlugins
              (plugins: map (x: plugins.${x}) [
                "c"
                "cpp"
                "go"
                "gomod"
                "haskell"
                "javascript"
                "lua"
                "make"
                "php"
                "python"
                "ruby"
                "rust"
                "sql"
              ]))

            # UI additions
            indent-blankline-nvim
            nvim-tree-lua
            nvim-web-devicons
            kanagawa-nvim
            noice-nvim

            # fuzzy finder
            telescope-nvim
            telescope-lsp-handlers-nvim
            telescope-ui-select-nvim
            telescope-ultisnips-nvim

            # language/highlighting integrations
            vim-docbk
            vim-javascript
            vim-jq
            vim-jsonnet
            vim-jsx-pretty
            vim-markdown
            vim-nix
            vim-terraform
            vim-toml
          ];
        };
      });

      vim-beam = addMainProgram "nvim" (final.buildEnv {
        name = "vim-beam";
        paths = [
          final.myvim
          (final.hiPrio (final.runCommand "vim-beam" { } ''
            mkdir -p $out/{libexec,bin}
            cat >$out/libexec/vim-wrapper <<EOF
            #! ${final.runtimeShell}
            ${final.myvim}/bin/"\$(basename "\$0")" "\$@"; e=$?
            printf '\033[6 q'
            exit $?
            EOF

            chmod a+x-w $out/libexec/vim-wrapper
            pushd $out/bin &>/dev/null
              for i in vi {,n}vim; do
                ln -sf "$out/libexec/vim-wrapper" "$out/bin/$i"
              done
            popd &>/dev/null
          ''))
        ];
      });

      ultisnipsDir = final.runCommand "ultisnips-dir" { } ''
        set -e
        mkdir $out
        cp -r ${./snippets}/* $out

        find $out/ -mindepth 1 -type d -print0 | while IFS= read -r -d $'\0' line; do
          chmod a+w -R "$line"
          cat "$line"/*.snippets > "$line".snippets
          rm -r "$line"
        done

        find $out -type f -print0 | while IFS= read -r -d $'\0' line; do
          all_snips="$(grep '^snippet' < $line | awk '{ print $2 }' | sort)"
          if [ "''$(wc -l <<< "$all_snips")" -ne "''$(uniq <<< "$all_snips" | wc -l)" ]; then
            echo -e "\e[31mSnippet file $line seems to contain duplicated snips!\e[0m"
            exit 1
          fi
        done
      '';

      vimPlugins = prev.vimPlugins // {
        # Patch to also show the commit hash in commit view
        vim-fugitive = prev.vimPlugins.vim-fugitive.overrideAttrs ({ patches ? [], ... }: {
          patches = patches ++ [ ./patches/fugitive-show-commit-hash-in-diff.patch ];
        });

        # Patch to show total line count in the location indicator
        lualine-nvim = prev.vimPlugins.lualine-nvim.overrideAttrs ({ patches ? [], ... }: {
          patches = patches ++ [ ./patches/lualine-total-length.patch ];
          doCheck = false;
        });

        # Don't map <CR> with two adjacent double quotes (breaks my workflow
        # when expanding a list of strings in Nix over multiple lines)
        nvim-autopairs = prev.vimPlugins.nvim-autopairs.overrideAttrs ({ patches ? [], ... }: {
          patches = patches ++ [ ./patches/autopairs-dont-map-cr-on-double-quote.patch ];
        });

        # Even with `hidden=true` exclude `.git` from search-results.
        telescope-nvim = prev.vimPlugins.telescope-nvim.overrideAttrs ({ patches ? [], ... }: {
          patches = patches ++ [ ./patches/telescope-exclude-dot-git-on-find-files.patch ];
        });

        # Bugfixes & indent fixes for Nix mode.
        vim-nix = prev.vimPlugins.vim-nix.overrideAttrs ({ patches ? [], ... }: {
          patches = patches ++ [
            ./patches/nix-disable-reformat-of-current-line-on-cr.patch
          ];
        });

        # Open URLs in the browser.
        gx-nvim = final.vimUtils.buildVimPlugin {
          name = "gx-nvim-${gx-nvim-src.lastModifiedDate}";
          src = gx-nvim-src;
        };

        # Highlighting for `jq`-exprs
        vim-jq = final.vimUtils.buildVimPlugin {
          pname = "vim-jq";
          version = vim-jq-src.lastModifiedDate;
          src = vim-jq-src;
        };

        # LSP configuration templates for neovim
        nvim-lspconfig = final.vimUtils.buildVimPlugin {
          pname = "nvim-lspconfig";
          version = nvim-lspconfig-src.lastModifiedDate;
          src = nvim-lspconfig-src;
        };

        # Way better than the `vim-markdown` package used in `nixpkgs`.
        vim-markdown = final.vimUtils.buildVimPlugin {
          name = "vim-markdown-${vim-markdown-src.lastModifiedDate}";
          src = vim-markdown-src;
        };

        # Markdown previewer integration
        vim-grip = final.vimUtils.buildVimPlugin {
          name = "vim-grip-${vim-grip-src.lastModifiedDate}";
          src = vim-grip-src;
        };
      };
    };

    hmModules.myvim = { config, lib, pkgs, ... }: with lib;
      let
        cfg = config.ma27.home.nvim;
      in {
        options.ma27.home.nvim = {
          enable = mkEnableOption "nvim.nix, an opinionated neovim distribution";
          package = mkOption {
            type = types.package;
            default = self.packages.${pkgs.stdenv.hostPlatform.system}.myvim;
            defaultText = literalExpression "self.packages.$${pkgs.stdenv.hostPlatform.system}.myvim";
            description = mdDoc ''
              Neovim package to use. Useful to optionally override the package in further ways.
            '';
          };
          defaultEditor = mkOption {
            type = types.bool;
            default = true;
            description = mdDoc ''
              Whether to make this editor the default editor in the system.
            '';
          };
          vimBeam = mkOption {
            type = types.bool;
            default = true;
            description = mdDoc ''
              Whether to reset the cursor to beam (small dash, like in insert mode) before
              returning to the interactive terminal when quitting `vim(1)` rather than keeping
              it a block (as it's in the normal mode).
            '';
          };
        };
        config = mkIf cfg.enable {
          ma27.home.nvim.package = mkIf cfg.vimBeam
            self.packages.${pkgs.stdenv.hostPlatform.system}.vim-beam;
          home.packages = [
            cfg.package
          ];
          home.sessionVariables = mkMerge [
            {
              NVIM_NIX_EXTRA_SNIPPETS_DIR = "${config.xdg.dataHome}/nvim-nix/extra-snippets";
            }
            (mkIf cfg.defaultEditor {
              EDITOR = "vim";
              GIT_EDITOR = "vim +start";
            })
          ];
        };
      };

    devShells = forEachSystem (system: {
      default = with nixpkgs.legacyPackages.${system}; mkShell {
        name = "nvim-test-env";
        packages = [
          neovim.unwrapped.lua
          lua-language-server
        ];
        inherit (self.checks.${system}.pre-commit-check) shellHook;
      };
    });

    checks = forEachSystem (system: {
      pre-commit-check = pre-commit.lib.${system}.run {
        src = ./.;
        hooks = {
          deadnix = {
            enable = true;
            excludes = [ "^templates/.*$" ];
          };
          custom-luacheck = {
            enable = true;
            pass_filenames = true;
            name = "luacheck";
            files = "\\.lua$";
            language = "system";
            entry = with nixpkgs.legacyPackages.${system};
              "${writeShellScript "luacheck" ''
                exec ${neovim.unwrapped.lua.pkgs.luacheck}/bin/luacheck "$@" --globals vim
              ''}";
          };
        };
      };
    });

    packages = forEachSystem (system:
      let
        legacyPackages' = nixpkgs.legacyPackages.${system}.extend (lib.composeManyExtensions [
          self.overlays.default
          nixd.overlays.default
        ]);
      in rec {
        inherit (legacyPackages') myvim vim-beam;
        default = vim-beam;
      });

    hydraJobs = {
      package = forEachHydraSystem (system: self.packages.${system}.myvim);
      lint = forEachHydraSystem (system: self.checks.${system}.pre-commit-check);
      release = forEachHydraSystem (system: with nixpkgs.legacyPackages.${system};
        runCommand "nvim-nix-release"
          {
            _hydraAggregate = true;
            constituents = [
              "package.${system}"
              "lint.${system}"
            ];
          }
          ''
            touch $out
          '');
    };

    templates = let
      mkLanguageInfo = name: tooling: ''
        Successfully generated a development environment for ${name} with the following
        components available in a dev shell:

        ${lib.concatMapStringsSep "\n" (x: "* ${x}") tooling}

        Happy hacking!
      '';
    in lib.mapAttrs (name: attrs: attrs // { path = ./templates/${name}; }) {
      go = {
        description = "Shell environment with Go & corresponding LSP";
        welcomeText = mkLanguageInfo "Go" [ "go compiler" "gopls" ] + ''
          To get started, do the following:

          * ensure that GOPATH points to `.go` in this directory
          * run `go mod vendor`
          * run e.g. `go build ./cmd/program`
        '';
      };
      haskell = {
        description = "Shell environment with GHC, general tooling and Haskell LSP Server";
        welcomeText = mkLanguageInfo "Haskell" [
          "GHC"
          "cabal"
          "hlint"
          "haskell-language-server"
        ];
      };
      php = {
        description = "Shell environment with PHP, dev tooling and phpactor for LSP";
        welcomeText = mkLanguageInfo "PHP" [
          "php (with apcu & xdebug)"
          "composer"
          "phpstan"
          "phpactor"
        ];
      };
      python = {
        description = "Shell environment with Python3 & LSP";
        welcomeText = mkLanguageInfo "Python" [
          "cpython"
          "python-lsp-server"
          "ruff"
          "mypy"
        ];
      };
      rust = {
        description = "Shell environment with rustc, cargo, dev tooling and rust-analyzer for LSP";
        welcomeText = mkLanguageInfo "Rust" [
          "rustc"
          "cargo"
          "rustfmt"
          "clippy"
          "rust-analyzer (as LSP)"
        ];
      };
      rust-crane = {
        description = "Development environment for new Rust projects, based on crane + cargo";
        welcomeText = ''
          Successfully scaffolded a new Rust project based on *crane*. To get started, please
          change the name of the project accordingly in `Cargo.toml`.

          * To add new dependencies, edit `Cargo.toml` and run `cargo update`.
          * To run the code, issue `nix run`.
          * To lint the code, use `nix flake check -L`.
        '';
      };
      tex = {
        description = "Tooling and shell environment with texlive for generating documents, papers etc..";
        welcomeText = builtins.replaceStrings
          [ "slides using beamer" ]
          [ "documents & papers" ]
          self.templates.tex-beamer.welcomeText;
      };
      tex-beamer = {
        description = "Tooling to work on / build slides using beamer";
        welcomeText = ''
          Successfully generated a TeX environment to build slides using beamer. This directory
          now consists of the following components:
          * texlive + latexmk for building
          * zathura for viewing

          The main entrypoint is `Index.tex`.

          Since this is generated from nvim-nix, no local development tooling is used,
          but it's expected to use vimtex for that purpose.

          By default, only a small portion of texlive packages is used. If more are needed,
          `tex-env.nix` can be regenerated like this:

              nix run .#update

          Building is also supported, a store-path with a rendered `Index.pdf` can
          be built by running

              nix build -L
        '';
      };
    };
  };
}
